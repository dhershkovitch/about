# About
My name is Dov Hershkovitch, and I'm a Product Manager at GitLab, focusing on monitoring.
Dov stands for a bear in Hebrew, my close friends calling me "doobie" (Yeah I know what you think), but "Doobie" stands for a teddy bear in Hebrew ;)


# General

* I've worked on monitoring throughout my entire career, starting at a company called [Mercury](https://en.wikipedia.org/wiki/Mercury_Interactive) which later got acquired by Hewlett Packard. 
* I spent a couple of years in [Elastic](https://www.elastic.co/) and fell in love working in a distributed companies.
* I am a firm believer in adapting for  change, whether its a new product or technology I never hesitate to learn and try. (however, sometimes I spend too much time on this).
* Over the years I learned (the hard way) the benefits of open source software, and I truly believe that open source is the future of software companies.


# How to reach me
* We work at a fully distributed company, I made sure my calendar is always up to date (even when I sleep). if you want a zoom meeting, find a spot in the calendar.
* The Israeli working week is Sunday to Thursday, Friday is a weekend (blocked on my calendar). 
* If you want to ask me something, I prefer Slack and Zoom meetings over emails.


# Personal
* I am based in Israel, in a city called [Beer Sheva](https://en.wikipedia.org/wiki/Beersheba), married + 3 kids.
* I am a fanatic soccer fan, and if my [soccer team](https://en.wikipedia.org/wiki/Hapoel_Be'er_Sheva_F.C.) plays well, I usually don't have a voice in the following morning.
* I like running, playing the guitar and spend time with my family.

[Linkedin](https://il.linkedin.com/in/dov0211) |
[Twitter](https://twitter.com/dov0211)
